import  { GetMessagesParamsType } from '../translates/entities';

export type CheckLetterParamsType = {
    id: number;
    letter: string;
} & GetMessagesParamsType;

export type ResponseCheckLetter = {
    status: boolean;
    positions: number[];
    letter: string;
}
