import { LangType } from 'entities';

export type MessagesType = {
    [key: string]: string;
}

export type GetMessagesParamsType = {
    lang?: LangType;
};
