export type AlphaBetViewType = string[];
export type KeyboardViewType = string[][];

export type ResponseLettersType = {
    letters: AlphaBetViewType;
    keyboardView: KeyboardViewType;
}
