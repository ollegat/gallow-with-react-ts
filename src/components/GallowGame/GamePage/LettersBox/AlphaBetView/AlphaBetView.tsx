import React from 'react';
import { useAppSelector } from 'store';
import { getLetters, getAlphaBetView } from 'store/letter/selectors';
import LetterButton from '../Letter';
import styles from './AlphaBetView.scss';

const AlphaBetView: React.FC = () => {

    const letters = useAppSelector(getLetters);
    const view = useAppSelector(getAlphaBetView);

    return <div className={styles.letters}>
        {
            view.map((item) => {
                return <LetterButton 
                    key={item}
                    isActive={letters[item]} 
                    symbol={item}
                />
            })
        }
    </div>;
};

export default AlphaBetView;
