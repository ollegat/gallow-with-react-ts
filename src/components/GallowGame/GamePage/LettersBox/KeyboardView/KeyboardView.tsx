import React from 'react';
import { useAppSelector } from 'store';
import { getLetters, getKeyboardView } from 'store/letter/selectors';
import LetterButton from '../Letter';
import styles from './KeyboardView.scss';

const prefix = 'keyboard-';
const KeyboardView: React.FC = () => {

    const letters = useAppSelector(getLetters);
    const view = useAppSelector(getKeyboardView);

    return <div className={styles.letters}>
        {
            view.map((item, key) => {
                return <div className={styles.column} key={key}>
                    {
                        item.map((letter) => {
                            return <LetterButton 
                                key={prefix+letter}
                                isActive={letters[letter]} 
                                symbol={letter}
                            />
                        })
                    }
                </div>;
            })
        }        
    </div>;
};

export default KeyboardView;
