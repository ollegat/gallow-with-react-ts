// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'bodyElement': string;
  'gallowsArmElement': string;
  'gallowsElement': string;
  'gallowsFoundElement': string;
  'gallowsRopeElement': string;
  'greyBodyElement': string;
  'greyGallowsArmElement': string;
  'greyGallowsElement': string;
  'greyGallowsFoundElement': string;
  'greyGallowsRopeElement': string;
  'greyHeadElement': string;
  'greyLeftHandElement': string;
  'greyLeftLegElement': string;
  'greyRightHandElement': string;
  'greyRightLegElement': string;
  'headElement': string;
  'leftHandElement': string;
  'leftLegElement': string;
  'rightHandElement': string;
  'rightLegElement': string;
  'wrap': string;
}
export const cssExports: CssExports;
export default cssExports;
