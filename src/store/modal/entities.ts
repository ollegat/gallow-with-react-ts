type GenericModal = {
    type: string;
    props: unknown;
};

export interface NoneModal extends GenericModal {
    type: 'None';
    props: null;
}

export interface WinModal extends GenericModal {
    type: 'Win';
    props: null;
}

export interface LoseModal extends GenericModal {
    type: 'Lose';
    props: null;
}

export type Modals =
    | NoneModal
    | WinModal
    | LoseModal;

export type ModalState = Modals;
