import { AlphaBetViewType, KeyboardViewType } from 'services/api/letters/entities';

export type Letters = {
    [key:string]: boolean;
}

export type LettersState = {
    letters: Letters;
    alphaBetView: AlphaBetViewType;
    keyboardView: KeyboardViewType;
}
