import { LangType } from 'entities';
import { MessagesType } from 'services/api/translates/entities';

export interface TranslatesState {
    messages: MessagesType;
    lang: LangType;
}
