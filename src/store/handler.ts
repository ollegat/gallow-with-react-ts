export const onReject = (error: unknown = ''): void => {
    console.error('rejected ', error);
};

export const NO_ERROR_MESSAGE = 'No error message';
