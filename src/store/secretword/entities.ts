
export interface SecretLetter {
    symbol: string;
    isOpen: boolean;
}

export interface SecretWord {
    [key:number]: SecretLetter;
}

export interface SecretWordState {
    id: number | null;
    countLetters: number;
    currentRiddle: string;
    currentSecretWord: SecretWord;
    isOpenWord: boolean;
    countOfError: number;
    isStart: boolean;
    isResetProogressbar: boolean;
}
