import "core-js/stable";
import "regenerator-runtime/runtime";
import * as React from "react";
import { render } from 'react-dom';
import App from './components/app';

render(<App />, document.querySelector('#root'));
