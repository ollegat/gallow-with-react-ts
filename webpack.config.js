const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const isProduction = Boolean(JSON.parse(process.env.NODE_ENV || 0));

module.exports = {
    mode: isProduction ? 'production' : 'development',
    entry: {
        app: './src/index.tsx'
    },
    output: {
        filename: 'bundle-[hash].js',
        path: path.resolve(__dirname, 'dist')
      },
    devtool: 'source-map',
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.css', '.scss'],
      alias: {
        'entities': path.resolve(__dirname, 'src/entities'),
        'services': path.resolve(__dirname, 'src/services'),
        'uikit': path.resolve(__dirname, 'src/uikit/'),
        'store': path.resolve(__dirname, 'src/store/'),
    }            
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000,
        watchContentBase: true,
        progress: true,
        open: true,
        allowedHosts: ['localhost:3000'],
        proxy: {
          '/api/**': 'http://localhost:3000',
        },
      }, 
      module: {
        rules: [
          {
            test: /\.(ts)x?$/,
            exclude: /node_modules/,
            use: [
              { loader: 'babel-loader'}
            ],
          },
          {
            test: /\.scss$/i,
            include: path.join(__dirname, 'src'),
            use: [
              { loader: "style-loader" },
              {
                loader: "css-modules-typescript-loader",
                options: {
                  sourceMap: true,
                },
              },               
              { loader: "css-loader",
                options: {
                  modules: {
                    exportLocalsConvention: 'camelCase',
                    localIdentName: isProduction
                        ? '[hash:base64:5]'
                        : '[name]__[local]___[hash:base64:5]',
                  },                  
                  sourceMap: true,
                }
              }, 
              { 
                loader: "sass-loader",
                options: {
                  additionalData: '@import "src/components/GallowGame/GamePage/Gallow/_paint.scss";'
                }
              }              
            ]
          },
          {
            test: /\.(png|svg|jpg|gif)$/,
            use: ["file-loader"]
          },
        ]
      },
      plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: '../dist/index.html',
            template: 'src/index.html'
        })
      ]
}